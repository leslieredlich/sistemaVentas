-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-07-2018 a las 19:58:33
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbsistemalaravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `id` int(10) UNSIGNED NOT NULL,
  `idcategoria` int(10) UNSIGNED NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `descripcion` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`id`, `idcategoria`, `codigo`, `nombre`, `precio_venta`, `stock`, `descripcion`, `condicion`, `created_at`, `updated_at`) VALUES
(1, 6, '014785239', 'Jabón de tocador', '1000.50', 300, NULL, 1, NULL, '2018-06-25 20:52:59'),
(2, 5, '36985214', 'Gaseosa Pepsi 1.5 LT', '2000.00', 200, NULL, 1, '2018-02-23 23:18:00', '2018-06-25 20:52:53'),
(5, 8, '0123644', 'Carne de pollo', '2450.00', 191, 'carnes de pollo y gallina', 1, '2018-02-23 23:18:57', '2018-06-25 20:54:16'),
(6, 5, '145236982', 'Gaseosa Inka Kola 2LT', '1500.00', 88, 'Gaseosa inka kola 2 litros', 1, '2018-02-23 23:26:52', '2018-06-25 20:52:31'),
(7, 5, '11223344', 'Pepsi', '1500.00', -335, 'Gaseosa Pepsi 600 ml', 1, '2018-02-24 11:09:45', '2018-06-25 20:53:20'),
(8, 5, '111222333', 'Coca-Cola', '1500.00', 110, 'Gaseosa CocaCola 1LT', 1, '2018-02-24 11:22:05', '2018-07-11 01:07:30'),
(9, 3, '8717868005604', 'Leche evaporada gloria 400g', '890.00', 201, 'Leche evaporada gloria tarro azul', 1, '2018-02-27 04:00:37', '2018-07-09 22:51:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `nombre`, `descripcion`, `condicion`, `created_at`, `updated_at`) VALUES
(1, 'Embutidos', 'Todo tipo de carnes rojas', 1, NULL, '2018-02-08 21:47:34'),
(2, 'Cereales', 'Todos los cereales', 0, NULL, '2018-02-08 21:37:55'),
(3, 'Lácteos', 'productos derivados de la leche', 1, '2018-02-08 06:45:53', '2018-02-08 06:45:53'),
(4, 'Menestras', 'todas las menestras', 1, '2018-02-08 06:53:41', '2018-02-08 06:53:41'),
(5, 'Bebidas', 'todas las bebidas', 1, '2018-02-08 06:54:04', '2018-02-08 06:54:04'),
(6, 'Artículos de Limpieza', 'Todos los artículos de limpieza', 1, '2018-02-08 07:36:55', '2018-02-08 20:04:48'),
(7, 'Carnes', 'Todas las carnes', 1, '2018-02-08 07:37:29', '2018-02-08 07:37:29'),
(8, 'Carnes blancas', 'todas las carnes blancas', 1, '2018-02-21 04:48:28', '2018-02-21 04:58:47'),
(9, 'Carnes cocidas', 'todas las carnes cocidas', 1, '2018-02-21 04:51:02', '2018-02-21 04:58:38'),
(10, 'Carnes procesadas', 'todas las carnes procesadas', 1, '2018-02-21 04:57:45', '2018-02-21 04:59:01'),
(11, 'Carnes de pescado', 'Todos los pescados', 1, '2018-02-21 05:00:05', '2018-02-21 05:00:05'),
(12, 'útiles escolares', 'Todos los artículos escolares', 1, '2018-02-21 10:22:46', '2018-06-14 21:35:54'),
(13, 'Muebles de oficina', 'todos los muebles de oficina pequeños', 0, '2018-02-21 22:07:00', '2018-07-09 22:51:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ingresos`
--

CREATE TABLE `detalle_ingresos` (
  `id` int(10) UNSIGNED NOT NULL,
  `idingreso` int(10) UNSIGNED NOT NULL,
  `idarticulo` int(10) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_ingresos`
--

INSERT INTO `detalle_ingresos` (`id`, `idingreso`, `idarticulo`, `cantidad`, `precio`) VALUES
(16, 6, 9, 100, '1500.00'),
(17, 6, 8, 100, '1000.00'),
(18, 7, 7, 100, '1000.00'),
(19, 8, 7, 100, '1000.00'),
(20, 9, 8, 100, '1400.00'),
(21, 10, 9, 100, '1000.00'),
(22, 10, 7, 100, '1000.00'),
(25, 13, 9, 1, '1.00'),
(26, 14, 6, 1, '1.00'),
(27, 14, 5, 1, '1.00'),
(28, 15, 7, 100, '1.00'),
(29, 15, 5, 100, '1.00'),
(30, 16, 1, 100, '1.00'),
(31, 16, 2, 100, '1.00'),
(32, 17, 6, 100, '1.00'),
(33, 18, 9, 10, '1000.00'),
(34, 18, 7, 10, '1000.00'),
(35, 19, 6, 1, '1.00'),
(36, 20, 9, 1, '1.00'),
(37, 21, 1, 100, '10.00'),
(38, 21, 2, 100, '10.00'),
(39, 22, 7, 10, '10.00'),
(40, 22, 8, 10, '10.00');

--
-- Disparadores `detalle_ingresos`
--
DELIMITER $$
CREATE TRIGGER `tr_updStockIngreso` AFTER INSERT ON `detalle_ingresos` FOR EACH ROW BEGIN
    UPDATE articulos SET stock = stock + NEW.cantidad
    WHERE articulos.id = NEW.idarticulo;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_ventas`
--

CREATE TABLE `detalle_ventas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idventa` int(10) UNSIGNED NOT NULL,
  `idarticulo` int(10) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` decimal(11,2) NOT NULL,
  `descuento` decimal(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detalle_ventas`
--

INSERT INTO `detalle_ventas` (`id`, `idventa`, `idarticulo`, `cantidad`, `precio`, `descuento`) VALUES
(1, 1, 7, 25, '1000.00', '0.00'),
(2, 2, 7, 10, '1500.00', '0.00'),
(3, 3, 5, 50, '2450.00', '10.00'),
(4, 3, 1, 50, '1000.50', '0.00'),
(5, 4, 7, 100, '1500.00', '0.00'),
(6, 4, 6, 100, '1500.00', '0.00'),
(7, 4, 2, 100, '2000.00', '15.00'),
(8, 5, 9, 10, '890.00', '0.00'),
(9, 6, 9, 1, '890.00', '0.00'),
(10, 7, 5, 10, '2450.00', '5.00'),
(11, 8, 6, 14, '1500.00', '0.00');

--
-- Disparadores `detalle_ventas`
--
DELIMITER $$
CREATE TRIGGER `tr_updStockVenta` AFTER INSERT ON `detalle_ventas` FOR EACH ROW BEGIN
    UPDATE articulos SET stock = stock - NEW.cantidad
    WHERE articulos.id = NEW.idarticulo;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ingresos`
--

CREATE TABLE `ingresos` (
  `id` int(10) UNSIGNED NOT NULL,
  `idproveedor` int(10) UNSIGNED NOT NULL,
  `idusuario` int(10) UNSIGNED NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_comprobante` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ingresos`
--

INSERT INTO `ingresos` (`id`, `idproveedor`, `idusuario`, `tipo_comprobante`, `serie_comprobante`, `num_comprobante`, `fecha_hora`, `impuesto`, `total`, `estado`, `created_at`, `updated_at`) VALUES
(6, 4, 11, 'BOLETA', '0001', '0001', '2018-04-09 00:00:00', '0.19', '250000.00', 'Registrado', '2018-05-28 03:04:59', '2018-05-28 03:04:59'),
(7, 3, 11, 'BOLETA', '0002', '0002', '2018-04-10 00:00:00', '0.19', '100000.00', 'Anulado', '2018-06-28 03:10:49', '2018-06-28 03:11:09'),
(8, 4, 11, 'BOLETA', '0003', '0003', '2018-05-23 00:00:00', '0.19', '100000.00', 'Registrado', '2018-06-28 04:03:07', '2018-06-28 04:03:07'),
(9, 4, 11, 'FACTURA', '0006', '0006', '2018-06-29 00:00:00', '0.19', '140000.00', 'Anulado', '2018-06-30 02:25:34', '2018-06-30 03:13:39'),
(10, 4, 11, 'BOLETA', '00007', '0007', '2018-07-04 00:00:00', '0.19', '200000.00', 'Anulado', '2018-06-30 03:05:17', '2018-06-30 03:06:41'),
(13, 3, 11, 'BOLETA', '333', '333', '2018-07-10 00:00:00', '0.19', '1.00', 'Registrado', '2018-07-10 21:25:15', '2018-07-10 21:25:15'),
(14, 4, 11, 'BOLETA', '3334', '3334', '2018-07-10 00:00:00', '0.19', '2.00', 'Registrado', '2018-07-10 22:16:24', '2018-07-10 22:16:24'),
(15, 3, 11, 'BOLETA', '0004', '0004', '2018-07-10 00:00:00', '0.19', '200.00', 'Registrado', '2018-07-10 22:17:05', '2018-07-10 22:17:05'),
(16, 3, 11, 'BOLETA', '555', '555', '2018-07-10 00:00:00', '0.19', '200.00', 'Registrado', '2018-07-10 22:18:22', '2018-07-10 22:18:22'),
(17, 4, 11, 'BOLETA', '2323', '2323', '2018-07-10 00:00:00', '0.19', '100.00', 'Registrado', '2018-07-10 22:35:51', '2018-07-10 22:35:51'),
(18, 4, 11, 'BOLETA', '01', '01', '2018-07-10 00:00:00', '0.19', '20000.00', 'Registrado', '2018-07-10 22:54:14', '2018-07-10 22:54:14'),
(19, 3, 11, 'BOLETA', '02', '02', '2018-07-10 00:00:00', '0.19', '1.00', 'Registrado', '2018-07-10 22:54:58', '2018-07-10 22:54:58'),
(20, 3, 11, 'BOLETA', '1212', '1212', '2018-07-10 00:00:00', '0.19', '1.00', 'Registrado', '2018-07-10 22:27:26', '2018-07-10 22:27:26'),
(21, 4, 11, 'BOLETA', '121212', '121212', '2018-07-11 00:00:00', '0.18', '2000.00', 'Registrado', '2018-07-11 14:50:14', '2018-07-11 14:50:14'),
(22, 3, 11, 'BOLETA', '1313', '1313', '2018-07-11 00:00:00', '0.18', '200.00', 'Registrado', '2018-07-11 14:56:05', '2018-07-11 14:56:05');

--
-- Disparadores `ingresos`
--
DELIMITER $$
CREATE TRIGGER `tr_updStockIngresoAnular` AFTER UPDATE ON `ingresos` FOR EACH ROW BEGIN
	UPDATE articulos a
    JOIN detalle_ingresos di
    ON di.idarticulo = a.id
    AND di.idingreso = new.id
    SET a.stock = a.stock - di.cantidad;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_02_01_171225_create_categorias_table', 1),
(4, '2018_02_21_151949_create_articulos_table', 2),
(5, '2018_02_22_235257_create_articulos_table', 3),
(6, '2018_02_27_143638_create_personas_table', 4),
(7, '2018_03_06_024616_create_proveedores_table', 5),
(8, '2018_03_13_133425_create_roles_table', 6),
(9, '2018_03_14_000000_create_users_table', 7),
(10, '2018_06_12_001852_create_ingresos_table', 8),
(11, '2018_06_12_001922_create_detalle_ingresos_table', 8),
(12, '2018_07_04_133931_create_ventas_table', 9),
(13, '2018_07_04_175454_create_detalle_ventas_table', 9),
(14, '2018_07_10_165852_create_notifications_table', 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('0812cdf1-c614-4ebf-ae20-3971e3609efc', 'App\\Notifications\\NotifyAdmin', 12, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:30:24', '2018-07-11 16:30:24'),
('11d87200-c2cf-4f33-ba74-585a9763fd52', 'App\\Notifications\\NotifyAdmin', 12, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 14:56:06', '2018-07-11 14:56:06'),
('125db6d2-d359-478a-a46d-6c50d68eac39', 'App\\Notifications\\NotifyAdmin', 13, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 14:50:17', '2018-07-11 14:50:17'),
('1bc96bdf-847d-4ed0-8234-8cde9c7e75ba', 'App\\Notifications\\NotifyAdmin', 11, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2018-07-30 16:40:51', '2018-07-11 16:26:30', '2018-07-30 16:40:51'),
('1f98552a-cfb4-4cea-941c-28a43faffa1d', 'App\\Notifications\\NotifyAdmin', 13, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:30:24', '2018-07-11 16:30:24'),
('2bff3b3b-48a1-4783-9e12-fdf2c9a96ce6', 'App\\Notifications\\NotifyAdmin', 11, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":2,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2018-07-30 16:40:51', '2018-07-11 16:30:23', '2018-07-30 16:40:51'),
('2f7dac73-cbcf-40af-9fe3-4d3cd7a2375f', 'App\\Notifications\\NotifyAdmin', 12, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:26:31', '2018-07-11 16:26:31'),
('4a8c332e-b14d-4841-8fc5-970a20f3a4f5', 'App\\Notifications\\NotifyAdmin', 12, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:36:26', '2018-07-11 16:36:26'),
('79ddd626-5023-4ea4-b817-a57b39969684', 'App\\Notifications\\NotifyAdmin', 13, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":1,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:26:31', '2018-07-11 16:26:31'),
('7a2eb321-8444-4110-92b2-e91dfe610e77', 'App\\Notifications\\NotifyAdmin', 12, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 14:50:17', '2018-07-11 14:50:17'),
('a48fe1b5-2884-412b-9e9c-5e9cb6077f93', 'App\\Notifications\\NotifyAdmin', 11, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2018-07-30 16:40:51', '2018-07-11 16:36:24', '2018-07-30 16:40:51'),
('a56b8483-dd50-4128-9564-b60433d5bf78', 'App\\Notifications\\NotifyAdmin', 11, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2018-07-30 16:40:51', '2018-07-11 16:37:01', '2018-07-30 16:40:51'),
('b910772d-5a52-4b32-bcb9-2d089bb2413b', 'App\\Notifications\\NotifyAdmin', 12, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:37:02', '2018-07-11 16:37:02'),
('b9823fc9-ba46-4143-a3f4-faa4dd7eb7cf', 'App\\Notifications\\NotifyAdmin', 13, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":3,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:36:26', '2018-07-11 16:36:26'),
('bc2df3db-bbd4-4b7b-bebf-8f1511d33f5b', 'App\\Notifications\\NotifyAdmin', 11, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":1,\"msj\":\"Ingresos\"}}}', '2018-07-30 16:40:51', '2018-07-11 14:50:15', '2018-07-30 16:40:51'),
('c2d31fad-3072-4506-a7ea-fdddb9dc4da8', 'App\\Notifications\\NotifyAdmin', 13, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":4,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 16:37:02', '2018-07-11 16:37:02'),
('ca6b8592-d9ae-48f9-942c-a7d9298922d1', 'App\\Notifications\\NotifyAdmin', 11, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', '2018-07-30 16:40:51', '2018-07-11 14:56:05', '2018-07-30 16:40:51'),
('fa43fe12-e311-4230-8314-36a23b8e7e49', 'App\\Notifications\\NotifyAdmin', 13, 'App\\User', '{\"datos\":{\"ventas\":{\"numero\":0,\"msj\":\"Ventas\"},\"ingresos\":{\"numero\":2,\"msj\":\"Ingresos\"}}}', NULL, '2018-07-11 14:56:06', '2018-07-11 14:56:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE `personas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `nombre`, `tipo_documento`, `num_documento`, `direccion`, `telefono`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Luis Arcila', 'DNI', '96325417', 'Josée Gálvez 1368 - Chongoyape', '931742905', 'luis.pad7@gmail.com', NULL, NULL),
(2, 'Leo Perez Dávila', 'DNI', '63214589', 'Zarumilla 113 - Salaverry', '963123523', 'leoperez@gmail.com', '2018-03-06 07:39:33', '2018-03-06 07:40:06'),
(3, 'Inversiones Leon SAC', 'RUC', '20154878961', 'AV. Lambayeque 150', '963254123', 'inversionesleon@gmail.com', NULL, NULL),
(4, 'Transportes MICASA SAA', 'RUC', '20145236982', 'Av. Atahualpa 122', '074562582', 'transportesmicasa@gmail.com', '2018-03-06 09:44:44', '2018-03-06 09:45:18'),
(8, 'Juan Carlos Arcila Diaz', 'DNI', '47715777', 'Zarumilla 113 - Chiclayo', '931742904', 'jcarlos.ad7@gmail.com', '2018-03-13 21:39:10', '2018-03-13 21:39:10'),
(9, 'Pedro Montenegro', 'DNI', '566778454', 'Calle Auroras', '3423', 'montenegro@gmail.com', '2018-03-21 23:58:35', '2018-03-22 00:05:29'),
(10, 'Maria Chocano', 'DNI', '678987675', 'San peter 2309', '55566655', 'maria@gmail.com', '2018-03-22 00:05:48', '2018-03-22 00:05:48'),
(11, 'leslie', 'DNI', '213123213', 'Calle nudo curauma #630, curauma, valparaiso', '0985230093', 'leslie@admin.cl', '2018-06-12 02:25:45', '2018-06-12 02:25:45'),
(12, 'Marcelo Rodriguez', 'DNI', '34534534', 'San isiadro 119', '12365489', 'vendedor@artic.cl', '2018-06-12 02:27:27', '2018-06-27 18:19:13'),
(13, 'Alberto Salazar', 'DNI', '234546342', 'los cactus 2025', '889448549', 'almacenero@artic.cl', '2018-06-12 02:28:18', '2018-06-27 18:20:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `id` int(10) UNSIGNED NOT NULL,
  `contacto` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_contacto` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`id`, `contacto`, `telefono_contacto`) VALUES
(3, 'Roberto Leon ', '98745632'),
(4, 'Rodolfo Gutierrez', '965874152');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `nombre` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `nombre`, `descripcion`, `condicion`) VALUES
(1, 'Administrador', 'Administradores de área', 1),
(2, 'Vendedor', 'Vendedor área venta', 1),
(3, 'Almacenero', 'Almacenero área compras', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `usuario` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condicion` tinyint(1) NOT NULL DEFAULT '1',
  `idrol` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuario`, `password`, `condicion`, `idrol`, `remember_token`) VALUES
(11, 'admin', '$2y$10$4Az9ape9DJljPtBt6AyRWO/pfvv.Yvh/IlysNU7lBdE1NrsaKRq2e', 1, 1, 'XSnx7TQlHJQDAQvF1PzEVBtgX0N2PSXLZ4hQi4a3gpBt3w6UjRHxrErTBGn2'),
(13, 'almacenero.salazar', '$2y$10$PR0eIEQOY8AKQbKMXjUbBuGGAJRCqa8nkYeqf9pkPA59e9rDQr0EO', 1, 3, '2r0UhrTQcT1OEeftU73rYSEksWwEOCSGkzEygx193nRNMaaVAtFGMbDdXWAk'),
(12, 'vendedor.rodriguez', '$2y$10$yd8LJxxjgI36SPm5.1iof.Q83GWGumCOtYe24EixQhSsqBlYUIJlC', 1, 2, 'Wlz961mpzdFuqMhvB8PCpRkWzOQrOlMfRWrQ1Ke0CyDHdwLWaz2e624uc0YB');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idcliente` int(10) UNSIGNED NOT NULL,
  `idusuario` int(10) UNSIGNED NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `num_comprobante` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`id`, `idcliente`, `idusuario`, `tipo_comprobante`, `serie_comprobante`, `num_comprobante`, `fecha_hora`, `impuesto`, `total`, `estado`, `created_at`, `updated_at`) VALUES
(1, 1, 12, 'BOLETA', '001', '0001', '2018-04-11 00:00:00', '0.19', '1000.00', 'Registrado', NULL, NULL),
(2, 1, 12, 'BOLETA', '111', '111', '2018-05-22 00:00:00', '0.19', '15000.00', 'Anulado', '2018-07-09 21:49:23', '2018-07-09 22:43:13'),
(3, 3, 12, 'BOLETA', '112', '112', '2018-06-28 00:00:00', '0.19', '172515.00', 'Anulado', '2018-07-09 22:16:30', '2018-07-09 22:35:53'),
(4, 10, 12, 'BOLETA', '1113', '11113', '2018-07-09 00:00:00', '0.19', '499985.00', 'Registrado', '2018-07-09 21:09:46', '2018-07-09 21:09:46'),
(5, 10, 11, 'BOLETA', '343434', '343434', '2018-07-11 00:00:00', '0.18', '8900.00', 'Registrado', '2018-07-11 16:26:30', '2018-07-11 16:26:30'),
(6, 1, 12, 'BOLETA', '121223', '121223', '2018-07-11 00:00:00', '0.18', '890.00', 'Registrado', '2018-07-11 16:30:23', '2018-07-11 16:30:23'),
(7, 4, 12, 'BOLETA', '656555', '55555', '2018-07-11 00:00:00', '0.18', '24495.00', 'Registrado', '2018-07-11 16:36:24', '2018-07-11 16:36:24'),
(8, 13, 12, 'BOLETA', '45454', '454545', '2018-07-11 00:00:00', '0.18', '21000.00', 'Registrado', '2018-07-11 16:37:01', '2018-07-11 16:37:01');

--
-- Disparadores `ventas`
--
DELIMITER $$
CREATE TRIGGER `tr_updStockVentaAnular` AFTER UPDATE ON `ventas` FOR EACH ROW BEGIN
	UPDATE articulos a
    JOIN detalle_ventas dv
    ON dv.idarticulo = a.id
    AND dv.idventa = new.id
    SET a.stock = a.stock + dv.cantidad;
END
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articulos_nombre_unique` (`nombre`),
  ADD KEY `articulos_idcategoria_foreign` (`idcategoria`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `detalle_ingresos`
--
ALTER TABLE `detalle_ingresos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_ingresos_idingreso_foreign` (`idingreso`),
  ADD KEY `detalle_ingresos_idarticulo_foreign` (`idarticulo`);

--
-- Indices de la tabla `detalle_ventas`
--
ALTER TABLE `detalle_ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detalle_ventas_idventa_foreign` (`idventa`),
  ADD KEY `detalle_ventas_idarticulo_foreign` (`idarticulo`);

--
-- Indices de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ingresos_idproveedor_foreign` (`idproveedor`),
  ADD KEY `ingresos_idusuario_foreign` (`idusuario`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personas_nombre_unique` (`nombre`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD KEY `proveedores_id_foreign` (`id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_nombre_unique` (`nombre`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_usuario_unique` (`usuario`),
  ADD KEY `users_id_foreign` (`id`),
  ADD KEY `users_idrol_foreign` (`idrol`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ventas_idcliente_foreign` (`idcliente`),
  ADD KEY `ventas_idusuario_foreign` (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `detalle_ingresos`
--
ALTER TABLE `detalle_ingresos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `detalle_ventas`
--
ALTER TABLE `detalle_ventas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `ingresos`
--
ALTER TABLE `ingresos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD CONSTRAINT `articulos_idcategoria_foreign` FOREIGN KEY (`idcategoria`) REFERENCES `categorias` (`id`);

--
-- Filtros para la tabla `detalle_ingresos`
--
ALTER TABLE `detalle_ingresos`
  ADD CONSTRAINT `detalle_ingresos_idarticulo_foreign` FOREIGN KEY (`idarticulo`) REFERENCES `articulos` (`id`),
  ADD CONSTRAINT `detalle_ingresos_idingreso_foreign` FOREIGN KEY (`idingreso`) REFERENCES `ingresos` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `detalle_ventas`
--
ALTER TABLE `detalle_ventas`
  ADD CONSTRAINT `detalle_ventas_idarticulo_foreign` FOREIGN KEY (`idarticulo`) REFERENCES `articulos` (`id`),
  ADD CONSTRAINT `detalle_ventas_idventa_foreign` FOREIGN KEY (`idventa`) REFERENCES `ventas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `ingresos`
--
ALTER TABLE `ingresos`
  ADD CONSTRAINT `ingresos_idproveedor_foreign` FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`id`),
  ADD CONSTRAINT `ingresos_idusuario_foreign` FOREIGN KEY (`idusuario`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD CONSTRAINT `proveedores_id_foreign` FOREIGN KEY (`id`) REFERENCES `personas` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_foreign` FOREIGN KEY (`id`) REFERENCES `personas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_idrol_foreign` FOREIGN KEY (`idrol`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD CONSTRAINT `ventas_idcliente_foreign` FOREIGN KEY (`idcliente`) REFERENCES `personas` (`id`),
  ADD CONSTRAINT `ventas_idusuario_foreign` FOREIGN KEY (`idusuario`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
